import os
FIELD_DIM=6

#reading field.txt and creating field of game
f=open('field.txt','r')
content=f.readlines()
f.close()

def creating_field(content,character,box):
	global field
	for i in range(len(content)):
		for j in range(len((content[i])[:-1])):
			if content[i][j]=='@':
				field[i][j]='.'
				character.x=i
				character.y=j
			elif content[i][j]=='$':
				field[i][j]='.'
				box.x=i
				box.y=j
			else:
				field[i][j]=content[i][j]

class MC(object):
	def __init__(self,x,y):
		self.x=x
		self.y=y
	def move(self,direction,box):
		global field
		global FIELD_DIM
		if direction==6:
			if field[self.x][self.y+1]!='#' and ((self.y+1!=box.y and self.x==box.x) or self.x!=box.x):
				self.y+=1
			elif self.y+1==box.y and self.x==box.x:
				if field[box.x][box.y+1]!='#':
					self.y+=1
					box.y+=1
		if direction==4:
			if field[self.x][self.y-1]!='#' and ((self.y-1!=box.y and self.x==box.x) or self.x!=box.x):
				self.y-=1
			elif self.y-1==box.y and self.x==box.x:
				if field[box.x][box.y-1]!='#':
					self.y-=1
					box.y-=1
		if direction==2:
			if field[self.x+1][self.y]!='#'  and ((self.x+1!=box.x and self.y==box.y) or self.y!=box.y):
				self.x+=1
			elif self.x+1==box.x and self.y==box.y:
				if field[box.x+1][box.y]!='#':
					self.x+=1
					box.x+=1
		if direction==8:
			if field[self.x-1][self.y]!='#'  and ((self.x-1!=box.x and self.y==box.y) or self.y!=box.y):
				self.x-=1
			elif self.x-1==box.x and self.y==box.y:
				if field[box.x-1][box.y]!='#':
					self.x-=1
					box.x-=1
class Crate(object):
	def __init__(self,x,y):
		self.x=x
		self.y=y

def printField(character,box):
	global field
	global FIELD_DIM
	for i in range(FIELD_DIM):
		for j in range(FIELD_DIM):
			if i==character.x and j==character.y:
				print('@',end='')
			elif i==box.x and j==box.y:
				print('$',end='') 
			else:
				print(field[i][j],end='')
		print()

field=[['.']*FIELD_DIM for i in range(FIELD_DIM)]
Character=MC(-1,-1)
Box=Crate(-1,-1)
creating_field(content,Character,Box)
running=True
print('Welcome to Sokoban!')
print('2,4,6,8-control')
print('@-you\n#-wall\n$-crate\n0-exit\n\n\'r\' to restart\n\'exit\' to exit')
printField(Character,Box)

while running:
	q=input()
	if q==str(1):
		os.system('clear')
		print('@-you\n#-wall\n$-crate\n0-exit\n\n\'r\' to restart\n\'exit\' to exit')
		printField(Character,Box)
	elif q==str(2) or q==str(4) or q==str(6) or q==str(8):
		os.system('clear')
		Character.move(int(q),Box)
		print('@-you\n#-wall\n$-crate\n0-exit\n\n\'r\' to restart\n\'exit\' to exit')
		printField(Character,Box)
	elif q=='r':
		os.system('clear')
		creating_field(content,Character,Box)
		print('@-you\n#-wall\n$-crate\n0-exit\n\n\'r\' to restart\n\'exit\' to exit')
		printField(Character,Box)
	elif q=='exit':
		running=False
	if field[Box.x][Box.y]=='0':
		print('Congratulations! You won this level! Buh bye!')
		running=False